import { BrowserProvider, ethers, toBeHex, getDefaultProvider } from "ethers";
import { CONTRACT_ADDRESSES } from "../constants/addresses";
import { networkMap } from "constants/networks";
import HyperCycleLicense from "../abis/HyperCycleLicense_POLYGON.json";
import MOCKERC20 from "../abis/MOCKERC20_ETHEREUM.json";
import HYPC_SWAP from "../abis/HYPC_SWAP_ETHEREUM.json";
import CHYPC from "../abis/CHYPC_ETHEREUM.json";

// Get the account in the window object
export async function getMetamaskInfo() {
  if (window.ethereum.isMetaMask) {
    const provider = new BrowserProvider(window.ethereum);
    const accounts = await provider.send("eth_requestAccounts", []);
    const chainId = (await provider.getNetwork()).chainId;
    const chainName = (await provider.getNetwork()).name;
    return {
      accounts,
      chainId,
      chainName,
    };
  }
}

export async function getLicensesContractInfo(chainId: string) {
  const provider = getDefaultProvider(
    networkMap[chainId as keyof typeof networkMap].rpcUrls[0]
  );
  const licensesContract = new ethers.Contract(
    CONTRACT_ADDRESSES[chainId as keyof typeof CONTRACT_ADDRESSES].license,
    HyperCycleLicense,
    provider
  );
  return Promise.all([
    licensesContract.getAddress(),
    licensesContract.name(),
    licensesContract.symbol(),
    licensesContract.deployTime(),
    licensesContract.count(),
    licensesContract.getNextPrice(),
    licensesContract.getNextPriceMATIC(),
  ]);
}

export async function getSwapContractInstance(chainId: any) {
  const provider = getDefaultProvider(
    networkMap[chainId as keyof typeof networkMap].rpcUrls[0]
  );
  const contract = new ethers.Contract(
    CONTRACT_ADDRESSES[chainId as keyof typeof CONTRACT_ADDRESSES].swap,
    HYPC_SWAP,
    provider
  );
  return contract;
}
export async function getLicenseContractInstance(chainId: any) {
  const provider = getDefaultProvider(
    networkMap[chainId as keyof typeof networkMap].rpcUrls[0]
  );
  const contract = new ethers.Contract(
    CONTRACT_ADDRESSES[chainId as keyof typeof CONTRACT_ADDRESSES].license,
    HyperCycleLicense,
    provider
  );
  return contract;
}

export async function getERC20ContractInstance(chainId: any) {
  const provider = getDefaultProvider(
    networkMap[chainId as keyof typeof networkMap].rpcUrls[0]
  );
  const contract = new ethers.Contract(
    CONTRACT_ADDRESSES[chainId as keyof typeof CONTRACT_ADDRESSES].erc20Mock,
    MOCKERC20,
    provider
  );
  return contract;
}

export async function getCHYPCContractInstance(chainId: any) {
  const provider = getDefaultProvider(
    networkMap[chainId as keyof typeof networkMap].rpcUrls[0]
  );
  const contract = new ethers.Contract(
    CONTRACT_ADDRESSES[chainId as keyof typeof CONTRACT_ADDRESSES].chypc,
    CHYPC,
    provider
  );
  return contract;
}

export async function getContractInfo(chainId: string) {
  let info: any = [];
  try {
    //Parallel execution of promises for fetching contract base information
    // Ethereum and polygon contracts may differ a bit in their interfaces
    if (chainId === "0x1" || chainId === "0xAA36A7") {
      // info = await getSwapContractInfo(chainId);
      info = [];
    } else {
      info = await getLicensesContractInfo(chainId);
    }
    return info;
  } catch (error) {
    console.log(error);
  }
}

export const isConnected = async () => {
  const accounts = await window.ethereum.request({ method: "eth_accounts" });
  return accounts.length ? true : false;
};

export const evmCompatibleConnector = async (chain: string) => {
  //Support multiple wallets -> MISSING
  //Get info from wallet (only metamask implemented)

  //Checking if there is a wallet installer in the browser
  if (!window.ethereum)
    throw new Error("No crypto wallet found. Please install it.");

  let contractInfo;
  let metamaskInfo = await getMetamaskInfo();

  //If current chain in wallet is different from the one selected
  if (metamaskInfo?.chainId.toString() != chain) {
    try {
      await window.ethereum.request({
        method: "wallet_switchEthereumChain",
        params: [{ chainId: chain }],
      });
    } catch (switchError: any) {
      // This error code indicates that the chain has not been added to MetaMask.
      if (switchError.code === 4902) {
        try {
          await window.ethereum.request({
            method: "wallet_addEthereumChain",
            params: [networkMap[chain as keyof typeof networkMap]],
          });
        } catch (addError) {
          console.log(addError);
          // handle "add" error
        }
      }
      console.log(switchError);
      // handle other "switch" errors
    }
    metamaskInfo = await getMetamaskInfo();
    contractInfo = await getContractInfo(chain);
  } else {
    //Get  contract info
    contractInfo = await getContractInfo(chain);
  }

  return {
    contractInfo,
    metamaskInfo,
  };
};

//only for mumbai right now
export const getLicenses = async (
  userAddress: string,
  contractAddress: string
) => {
  console.log("Getlicense inputs", userAddress, contractAddress);
  const provider = getDefaultProvider(networkMap["0x13881"].rpcUrls[0]);
  const lincenseContract = new ethers.Contract(
    contractAddress,
    HyperCycleLicense,
    provider
  );

  const balanceOf: Number = await lincenseContract.balanceOf(userAddress);
  // Concurrent asynchronous petition
  let promises = [];
  for (let i = 0; i < Number(balanceOf); i++) {
    promises.push(lincenseContract.tokenOfOwnerByIndex(userAddress, i));
  }
  let tokenIds = await Promise.all(promises);
  return tokenIds;
};

// Only valid using ERC721Enumerable extension from OZ contracts
export const getNFTIds = async (userAddress: string, contractInstance: any) => {
  const balanceOf: Number = await contractInstance.balanceOf(userAddress);
  // Concurrent asynchronous petition
  let promises = [];
  for (let i = 0; i < Number(balanceOf); i++) {
    promises.push(contractInstance.tokenOfOwnerByIndex(userAddress, i));
  }
  let tokenIds = await Promise.all(promises);
  return tokenIds;
};
// Only valid using ERC721Enumerable extension from OZ contracts
export const getNFTIdsAssignments = async (
  userAddress: string,
  contractInstance: any
) => {
  let assignments;
  try {
    
    const balanceOf: Number = await contractInstance.balanceOf(userAddress);
    // Concurrent asynchronous petition
    let promises = [];
    for (let i = 0; i < Number(balanceOf); i++) {
      promises.push(contractInstance.tokenOfOwnerByIndex(userAddress, i));
    }
    let tokenIds = await Promise.all(promises);
    let secondPromises = [];
    for (let i = 0; i < tokenIds.length; i++) {
      secondPromises.push(contractInstance.getAssignment(tokenIds[i]));
    }
    const assigned = await Promise.all(secondPromises);
    assignments = tokenIds.map((id, i) => {
      return {
        id: id,
        assigned: assigned[i],
      };
    });
  } catch (error) {
    console.log(error)
  }
  return assignments;
};
