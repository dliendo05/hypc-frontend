export const networkMap = {
    "0x89": {
      chainId: "0x89", // '0x89'
      chainName: "Matic(Polygon) Mainnet",
      nativeCurrency: { name: "MATIC", symbol: "MATIC", decimals: 18 },
      rpcUrls: ["https://polygon-rpc.com"],
      blockExplorerUrls: ["https://www.polygonscan.com/"],
    },
    "0x13881": {
      chainId: "0x13881", // '0x13881'
      chainName: "Matic(Polygon) Mumbai Testnet",
      nativeCurrency: { name: "tMATIC", symbol: "tMATIC", decimals: 18 },
      rpcUrls: ["https://rpc.ankr.com/polygon_mumbai"],
      blockExplorerUrls: ["https://mumbai.polygonscan.com/"],
    },
    "0x1": {
      chainid: "0x01",
      chainName: "Ethereum",
      nativeCurrency: { name: "Ethereum Mainnet", symbol: "ETH", decimals: 18},
      rpcUrls : ["https://eth.llamarpc.com"],
      blockExplorerUrls: ["https://etherscan.io"]
    },
    "0xAA36A7": {
      chainid: "0xAA36A7",
      chainName: "Sepolia",
      nativeCurrency: { name: "Ethereum Sepolia", symbol: "SepoliaETH", decimals: 18},
      rpcUrls : ["https://rpc.sepolia.org"],
      blockExplorerUrls: ["https://etherscan.io"]
    }
  };
  
