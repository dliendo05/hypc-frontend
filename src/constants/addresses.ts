export const HyperCycleLicense_Contract = {
  "0x13881": "0x94a128011E40F235C29a24748D3E1c95FC2DA071",
  "0x1": "0x8d8503Ed56BE90bFF89FB716A3f9e0C359EE03ED",
  "0x89": "",
};

export const HYPC_Swap = {
  "0x01": "",
  "0xAA36A7": "",
};

export const CONTRACT_ADDRESSES: any = {
  //Polygon:
  "0x89": {},
  //Ethereum
  "0x01": {},
  //Mumbai
  "0x13881": {
    license: "0x94a128011E40F235C29a24748D3E1c95FC2DA071",
  },
  //Sepolia
  "0xAA36A7": {
    swap: "0x9CD8F298F349A1C2E985B1967FC0242ef28518fd",
    chypc: "0x5391636Fd0E141117e77518553Efd29d6FAD1525",
    erc20Mock: "0x5aFFa4FBADab6011e7b19eF820C9Ea865B2B6f59",
  },
};
