import { useEffect, useState } from "react";
import "./global.css";
import "./App.css";

import { getMetamaskInfo, getContractInfo, evmCompatibleConnector, isConnected } from "./connection/index";
import { BigNumberish, formatEther, formatUnits } from "ethers";
import {LicenseContract, ContractData} from "components/cards/LicenseContract";
import Navbar from "components/layout/Navbar";
import Layout from "components/layout";

interface IUserInfo {
  key: string;
  network: string;
  chainId: BigNumberish | undefined;
}

const App = () => {
  const [userInfo, setUserInfo] = useState<IUserInfo | undefined>()
  const [chainInfo, setChainInfo] = useState<string | undefined>()
  const [contractInfo, setContractInfo] = useState<any | undefined>()

  useEffect(() => {
    const fetchInfo = async () => {
      // 
      // const info = await getContractInfo();
      // setContractInfo(info);
    }
    // fetchInfo()0;
  }, [])


  // Implement multiple connectors for multiple wallets
  // const metamaskConnector = async () => {
  //   const info = await getMetamaskInfo();
  //   setUserInfo({
  //     key: info!.accounts[0],
  //     network: info!.chainName,
  //     chainId: info!.chainId,
  //   })
  // }

  const connect = async(chain: string) => {
    const { contractInfo, metamaskInfo} = await evmCompatibleConnector(chain);
    setUserInfo({
      key: metamaskInfo!.accounts[0],
      network: metamaskInfo!.chainName,
      chainId: metamaskInfo!.chainId
    })
    setContractInfo(
     contractInfo
    )
  }

  return (
    <div className="App">
      <Layout>

      {userInfo == undefined ? <div>
        <div className="mb-3 flex flex-col">
          <div className="flex space-x-5">
            {/* <button type="button" className="inline-flex items-center px-2.5 py-1.5 border border-transparent text-md font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" onClick={() =>  connect ('0x89')}> Polygon</button>
            <button type="button" className="inline-flex items-center px-2.5 py-1.5 border border-transparent text-md font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" onClick={() => connect ('0x13881')}> Mumbai Testnet</button>
            <button type="button" className="inline-flex items-center px-2.5 py-1.5 border border-transparent text-md font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" onClick={() => connect ('0x1')}> Ethereum</button> */}
            {/* <button type="button" className="inline-flex items-center px-2.5 py-1.5 border border-transparent text-md font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" onClick={() => cardanoCompatibleConnector("cardano_mainnet")}> Cardano</button> */}
          </div>
        </div>

        {/* Different wallets for different chains missing */}
        {/* <div className="flex flex-col">
          <span className="text-xl mb-3">
            Select a wallet:
          </span>
          <div>
          <button type="button" className="inline-flex items-center px-2.5 py-1.5 border border-transparent text-md font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" onClick={metamaskConnector}> Metamask</button>
          </div>
        </div> */}
      </div> :
        <div className="flex flex-col">
          <span className="py-5">
            Wallet: {userInfo.key}</span>
          <span className="">
            Current chain: {userInfo.network}
          </span>
        </div>
      }
      {
        contractInfo == undefined ? '' :
        <LicenseContract address = {contractInfo[0]} 
        name = {contractInfo[1]}
        symbol = {contractInfo[2]}
        count = {contractInfo[4].toString()}
        nextPrice = {contractInfo[5].toString()}
        nextPriceBaseToken = {contractInfo[6].toString()} />
      }
      </Layout>
    </div>
  );
};

export default App;
