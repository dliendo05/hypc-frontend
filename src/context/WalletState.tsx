import { createContext, useReducer } from "react";
import { walletReducer, SET_WALLET } from "./reducers";

const initialState: any = {
    wallet: {
        accounts:[]
    },
    userLicenses: []
}

export const WalletContext = createContext(initialState)

export const WalletProvider = ({children}:any) =>  {
    const [state, dispatch] = useReducer(walletReducer, initialState)

    return <WalletContext.Provider value={{state, dispatch}} >{children}</WalletContext.Provider> ;
}