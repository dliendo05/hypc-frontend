// Type variables

// Action typing
type ActionMap<M extends { [index: string]: any }> = {
  [Key in keyof M]: M[Key] extends undefined
    ? {
        type: Key;
      }
    : {
        type: Key;
        payload: M[Key];
      };
};

export const SET_WALLET = "SET_WALLET";
export const SET_USER_LICENSES = "SET_USER_LICENSES";
export const walletReducer = (state: any, action: any) => {
    console.log(action.payload, "WALLET REDUCER CALLED", action.type)

  switch (action.type) {
    case SET_WALLET: {
      return {
        ...state,
        wallet: action.payload,
      };
    }
    case SET_USER_LICENSES: {
        console.log(action.payload, "USER LICENSES CALLED")
        return {
            ...state,
            userLicenses: action.payload
        }
    }
    default:
      return state;
  }
};

// Contracts data from blockchain
export const SET_CONTRACTS = "SET_CONTRACTS";
export const contractsReducer = (state: any, action: any) => {
  switch (action.type) {
    case SET_CONTRACTS: {
      console.log(" I WAS CALLED", action, state);
      return {
        ...state,
        contracts: action.payload,
      };
    }
    default:
      return state;
  }
};
