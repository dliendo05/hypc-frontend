import { createContext, useReducer } from "react";
import { contractsReducer, SET_CONTRACTS } from "./reducers";

const initialState: any = {
    blockchain: {},
    contracts: []
}

export const BlockchainContext = createContext(initialState)

export const BlockchainProvider = ({ children }: any) => {
    const [state, dispatch] = useReducer(contractsReducer, initialState)

    return <BlockchainContext.Provider value={{ state, dispatch }} >{children}</BlockchainContext.Provider>;
}