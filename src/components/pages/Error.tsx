import React, { useState, useEffect } from 'react';

// interface ErrorProps {
//   error  : string;
// }
import './Error.css';

const Error: React.FC = () => {
    const [, set] = useState<string>();
    // useEffect(() => {
    //     set();
    // }, []);
  const [errorCode, setErrorCode] = useState<string>();

  useEffect(() => {
    // Get the error code from the URL query parameters
    const urlSearchParams = new URLSearchParams(window.location.search);
    const params = Object.fromEntries(urlSearchParams.entries());
    const error = params.error;

    // Set the error code
    setErrorCode(error || '404');
  }, []);

  return (
    <div className="error-container">
      <h1 className="error-heading">Error {errorCode}</h1>
      <p className="error-message">Oops! Looks like the page you're trying to access doesn't exist.</p>
      <p className="error-message">Please check the URL, go back to the homepage or the previous page you were on.</p>
      <div className="error-buttons">
        <button className="button" onClick={() => window.location.href = '/'}>Go to home</button>
        <button className="button" onClick={() => window.history.back()}>Previous page</button>
      </div>
    </div>
  );
};

export default Error;
