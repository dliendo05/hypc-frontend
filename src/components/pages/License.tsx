import React, { useState, useEffect, useContext, useLayoutEffect } from 'react';
import { LicenseContract } from 'components/cards/LicenseContract';
import { BlockchainContext } from 'context/BlockchainState';
import { getContractInfo, getLicenses } from 'connection';
import { SET_CONTRACTS, SET_USER_LICENSES } from 'context/reducers';
import { WalletContext } from 'context/WalletState';
import SplitLicense from 'components/cards/SplitLicense';
import { HyperCycleLicense_Contract } from 'constants/addresses';

// interface LicenseProps {
//     // : string;
// }

const License: React.FC = () => {
    const [, set] = useState<string>();
    const { state, dispatch } = useContext(BlockchainContext)
    const walletContext = useContext(WalletContext)
    useLayoutEffect(() => {
        let contracts
        const contractInfo = async () => {
            contracts = await getContractInfo('0x13881')
            dispatch({
                type: SET_CONTRACTS,
                payload: contracts
            })
        }
        contractInfo();
    }, [])
    return (
        <>
            <div className='flex flex-col justify-center items-center min-h-screen'>
                {state.contracts.length > 0 ?
                    <LicenseContract address={state.contracts[0]}
                        name={state.contracts[1]}
                        symbol={state.contracts[2]}
                        count={state.contracts[4].toString()}
                        nextPrice={state.contracts[5].toString()}
                        nextPriceBaseToken={state.contracts[6].toString()} />
                    :
                    ""
                }
                <SplitLicense />
            </div>
        </>
    );
};

export default License;
