import Assigner from 'components/cards/Assigner';
import Swaper from 'components/cards/Swaper';
import Layout from 'components/layout';
import Navbar from 'components/layout/Navbar';
import React, { useState, useEffect } from 'react';

// interface SwapProps {
//     // : string;
// }

const Swap: React.FC = () => {


    return (
        <>
           <div className='flex flex-col justify-center items-center min-h-screen'>
            <Swaper />
            <Assigner />
           </div>
        </>
    );
};

export default Swap;
