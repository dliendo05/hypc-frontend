import MintMockERC20 from 'components/cards/MintMockERC20';
import Layout from 'components/layout';
import Navbar from 'components/layout/Navbar';
import React, { useState, useEffect } from 'react';

// interface SwapProps {
//     // : string;
// }

const Mint: React.FC = () => {
    const [, set] = useState<string>();
    // useEffect(() => {
    //     set();
    // }, []);

    return (
        <>
           <div className='flex flex-col justify-center items-center min-h-screen'>
            <MintMockERC20 />
           </div>
        </>
    );
};

export default Mint;
