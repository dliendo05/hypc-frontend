import React, { useState, useEffect, useContext } from 'react';
import LabeledDivider from 'components/utils/dividers/LabeledDivider';
import { WalletContext } from 'context/WalletState';
import { getCHYPCContractInstance, getERC20ContractInstance, getNFTIds, getSwapContractInstance } from 'connection';
import { ethers } from 'ethers';
import { CONTRACT_ADDRESSES } from 'constants/addresses';
import Loading from 'components/buttons/Loading';


const Swaper: React.FC = () => {
    const provider = new ethers.BrowserProvider(window.ethereum);
    const [isApproved, setIsApproved] = useState<boolean>(false);
    const [successfullTx, setSuccessfullTx] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const [parsedLogs, setParsedLogs] = useState();
    const [erc20State, setErc20State] = useState({
        balance: 0,
        symbol: ""
    });
    const [cHYPCState, setCHYPCState] = useState<any>({
        // counter: 0,
        balance: 0,
        symbol: "",
        // ids: []
    });
    const walletContext = useContext(WalletContext);
    const [erc20Input, setERC20Input] = useState('');
    const [nftInput, setNFTInput] = useState('');
    useEffect(() => {
        const getBalances = async (user: any) => {
            try {
                const erc20Instance = await getERC20ContractInstance("0xAA36A7")
                const erc20Promises = await Promise.all([erc20Instance.balanceOf(user), erc20Instance.symbol()])
                setErc20State({
                    balance: erc20Promises[0],
                    symbol: erc20Promises[1]
                })
            } catch (error) {
                console.log(error)
            }
        }
        if (walletContext.state.wallet.accounts.length > 0) {
            getBalances(walletContext.state.wallet.accounts[0]);
        }
    }, [walletContext.state.wallet.accounts, successfullTx])
    useEffect(() => {
        const getBalances = async (user: any) => {
            try {
                const cHYPCInstance = await getCHYPCContractInstance("0xAA36A7")
                // const nftIds = await getNFTIds(walletContext.state.wallet.accounts[0], cHYPCInstance);
                const promises = await Promise.all([cHYPCInstance.balanceOf(user), cHYPCInstance.symbol()])
                setCHYPCState({
                    balance: promises[0],
                    symbol: promises[1],
                    // ids: promises[2]
                })
            } catch (error) {
                console.log(error)
            }
        }
        if (walletContext.state.wallet.accounts.length > 0) {
            getBalances(walletContext.state.wallet.accounts[0]);
        }
    }, [walletContext.state.wallet.accounts, successfullTx])

    const erc20InputOnChange = (e: any) => {
        const value = e.replace(/[^0-9.]|(?<=\..*)\./g, "")
        setERC20Input(value)
    }
    const nftInputOnChange = (e: any) => {
        const value = e.replace(/[^0-9.]|(?<=\..*)\./g, "")
        setNFTInput(value)
    }
    useEffect(() => {
        // Early returns for state inconsistencies
        if (walletContext.state.wallet.accounts.length == 0) return;
        let swapInstance: ethers.Contract;
        const getEvents = async () => {
            swapInstance = await getSwapContractInstance("0xAA36A7");
            //Get swaps for the user 
            const filterSwap = swapInstance.filters.Swap(walletContext.state.wallet.accounts[0])
            swapInstance.on(filterSwap, (data: any) => {
                console.log(data, "Data from the event swap")
            })
        }
        getEvents();
        return () => {
            if (swapInstance === undefined) return;
            swapInstance.removeAllListeners()
        }
    }, [])
    useEffect(() => {
        // Early returns for state inconsistencies
        if (walletContext.state.wallet.accounts.length == 0) return;
        let erc20Instance: ethers.Contract;
        const getEvents = async () => {
            erc20Instance = await getERC20ContractInstance("0xAA36A7");
            //Get approvals for the user 
            const filterApproval = erc20Instance.filters.Approval(walletContext.state.wallet.accounts[0])
            erc20Instance.on(filterApproval, (data: any) => {
                console.log(data, "Data from the event approve")
            })
        }
        getEvents();
        return () => {
            if (erc20Instance === undefined) return;
            erc20Instance.removeAllListeners()
        }
    }, [])

    const approve = async () => {
        setIsLoading(!isLoading);
        try {
            const signer = await provider.getSigner();
            const erc20Instance: any = await getERC20ContractInstance("0xAA36A7")
            const swapInstance: any = await getSwapContractInstance("0xAA36A7");
            let approveTx = await erc20Instance.connect(signer).approve(await swapInstance.getAddress(), ethers.parseEther("524288"))
            const approveReceipt = await (approveTx.wait());
            setIsLoading(false);
            // For successful transaction
            if (approveReceipt.status) {
                setIsApproved(true)
                setSuccessfullTx(!successfullTx)
            } else {
                console.log("Tx did not passed")
            }
            ///Implement unsuccessful transaction response
        } catch (error) {
            console.log(error)
        }
    }

    const swap = async () => {
        setIsLoading(!isLoading)
        try {
            const signer = await provider.getSigner();
            const swapInstance: any = await getSwapContractInstance("0xAA36A7");
            let swapTx = await swapInstance.connect(signer).swap()
            const swapReceipt = await swapTx.wait();
            setIsLoading(false);
            // For successful transaction
            if (swapReceipt.status) {
                setIsApproved(false)
                setSuccessfullTx(!successfullTx)
            }
            ///Implement unsuccessful transaction code
            else {
                console.log("Tx did not passed")
            }
        } catch (error) {
            console.log(error)
        }
        return;
    }
    const redeem = async (tokenId: any) => {
        try {
            const signer = await provider.getSigner();
            const swapInstance: any = await getSwapContractInstance("0xAA36A7");
            // const erc20Instance: any = await getERC20ContractInstance("0xAA36A7")
            // await erc20Instance.connect(signer).approve(await swapInstance.getAddress(), ethers.parseEther("524288"))
            await swapInstance.connect(signer).redeem(tokenId)
        } catch (error) {
            console.log(error)
        }
        return;
    }
    return (
        <>
            <div className='bg-[#162031]  w-[462px] min-h-[380px]  px-6 py-4 rounded-lg flex flex-col  items-center border border-gray-700 shadow-md shadow-gray-500/50 mb-4'>
                <div className='text-xl w-full text-left font-bold h-11 flex items-center mb-5'>
                    Swap
                </div>
                <div className='bg-[#1e293b] w-full m-[1px] rounded-xl hover:m-0 hover:border border-gray-500 px-[13px] py-2'>
                    <div className='flex justify-between items-center mb-2'>
                        <div className='bg-[#334155] rounded-lg px-3 py-2'>
                            {erc20State.symbol}
                        </div>
                        <div className='flex flex-col'>
                            <span className='w-full text-right text-lg'>
                                524288
                            </span>
                            <span className='text-sm font-extralight'>
                                Balance: {erc20State.balance.toString()}
                            </span>
                        </div>
                        {/* <input value={erc20Input} onChange={e => erc20InputOnChange(e.target.value)} type="text" name="token_out" autoComplete='off' autoCorrect='off' spellCheck="false" aria-label='tokenOut' placeholder="0.00" defaultValue={"0.00"} className='bg-[#1e293b] text-xl text-right outline-none input-number text-gray-300' /> */}
                    </div>
                    {/* <div className='flex justify-between text-sm text-gray-400'>
                        <span>
                            Balance: {erc20State.balance.toString()}
                        </span>
                        <span className=''>
                            $1.87
                        </span>
                    </div>
                    Conditional Error, error message for input or balance, user input related only
                    <div className='w-full text-red-600 text-[14px]'>
                        Error message
                    </div> */}
                </div>
                <LabeledDivider label={"for"} />
                <div className='bg-[#1e293b] w-full m-[1px] rounded-xl hover:m-0 hover:border border-gray-500 px-[13px] py-2'>
                    <div className='flex justify-between items-center mb-2'>
                        <div className='bg-[#334155] rounded-lg px-3 py-2'>
                            {cHYPCState.symbol}
                        </div>
                        <div className='flex flex-col'>
                            <span className='w-full text-right text-lg'>
                                {/* Id: {cHYPCState.counter.toString()} */}
                                1
                            </span>
                            <span className='text-sm font-extralight'>
                                Total Balance: {cHYPCState.balance.toString()}
                            </span>
                        </div>
                        {/* <input value={nftInput} onChange={e => nftInputOnChange(e.target.value)} type="text" name="token_out" autoComplete='off' autoCorrect='off' spellCheck="false" aria-label='tokenOut' placeholder="0.00" defaultValue={"0.00"} className='bg-[#1e293b] text-xl text-right outline-none input-number text-gray-300' /> */}
                    </div>
                    {/* <div className='flex justify-between text-sm text-gray-400'>
                        <span>
                            Balance: {cHYPCState.balance.toString()}
                        </span>
                        <span className=''>
                            $1.87
                        </span>
                    </div>
                    Conditional Error, error message for input or balance, user input related only
                    <div className='w-full text-red-600 text-[14px]'>
                        Error message
                    </div> */}
                </div>
                {/* conditional Error, transaction error */}
                <div className='mb-3 w-full text-left text-red-600'>
                    {/* Error message */}
                </div>
                <div className='flex flex-col items-center justify-center w-full'>
                    {
                        isLoading ?
                            <Loading classes={`w-full items-center flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-lg font-medium text-white bg-gradient-to-tr from-purple-600 to-blue-500 cursor-not-allowed`} size={'6'} />
                            :
                            isApproved ?
                                (<button
                                    onClick={swap}
                                    type="submit"
                                    className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-lg font-medium text-white bg-gradient-to-tr from-purple-600 to-blue-500 hover:from-purple-500 hover:to-blue-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-violet-500"
                                >
                                    Swap
                                </button>)
                                :
                                (<button
                                    onClick={approve}
                                    type="submit"
                                    className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-lg font-medium text-white bg-gradient-to-tr from-purple-600 to-blue-500 hover:from-purple-500 hover:to-blue-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-violet-500"
                                >
                                    Approve
                                </button>)
                    }

                    <span className='text-sm font-light text-gray-300 w-full mt-1 text-left'>
                        Remember: Only 1 CHYPC NFT for 524288 HYPC tokens
                    </span>
                </div>
            </div>
        </>
    );
};

export default Swaper;
