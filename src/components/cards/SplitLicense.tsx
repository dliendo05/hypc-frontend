import { getLicenseContractInstance, getLicenses } from 'connection';
import { HyperCycleLicense_Contract } from 'constants/addresses';
import { SET_USER_LICENSES } from 'context/reducers';
import { WalletContext } from 'context/WalletState';
import { ethers } from 'ethers';
import React, { useState, useEffect, useContext } from 'react';

// interface SplitLicenseProps {
//     : string;
// }

export const SplitLicense: React.FC = () => {
    const [, set] = useState<string>();
    const walletContext = useContext(WalletContext)

    const split = async (license: string) => {
        const provider = new ethers.BrowserProvider(window.ethereum);
        const signer = await provider.getSigner();
        const licenseInstance: any = await getLicenseContractInstance("0x13881");
        const splitTx = await licenseInstance.connect(signer).splitToken(license);
        const splitReceipt = await splitTx.wait();
        if (splitReceipt.status) {
            const licensesId = await getLicenses(walletContext.state.wallet.accounts[0], HyperCycleLicense_Contract['0x13881'])
            walletContext.dispatch({ type: SET_USER_LICENSES, payload: licensesId })
        }
    }
    return (
        <>
            <div className='border border-gray-700 shadow-md shadow-gray-500/50  w-[512px] rounded-md py-7 px-5 text-white text-start bg-[#162031]'>
                <div className='flex flex-col justify-center items-center text-xl'>
                    <span className='w-full text-left mb-4'>User's licenses: </span>
                    {walletContext.state.userLicenses.length > 0 ? walletContext.state.userLicenses.map((license: string) => {
                        return (
                            <div key={license.toString()} className="flex items-center text-left w-64 mb-3 border border-gray-500 py-2 px-4 rounded-md shadow-md shadow-gray-500/50">
                                <span className='text-white mr-2 text-base'>
                                    Id: {license.toString()}
                                </span>
                                <button
                                    onClick={() => split(license.toString())}
                                    className="flex justify-center py-1 px-4 border border-transparent rounded-md shadow-sm text-base font-light text-white bg-gradient-to-tr from-purple-600 to-blue-500 hover:from-purple-500 hover:to-blue-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-violet-500"
                                >
                                    Split
                                </button>
                            </div>)
                    }) : ""}
                </div>
            </div>
        </>
    );
};

export default SplitLicense;
