import { getCHYPCContractInstance, getNFTIds, getNFTIdsAssignments, getSwapContractInstance } from 'connection';
import { WalletContext } from 'context/WalletState';
import { ethers } from 'ethers';
import React, { useContext, useEffect, useState } from 'react';

// interface AssignerProps {
//     : string;
// }

const Assigner: React.FC = () => {
    const provider = new ethers.BrowserProvider(window.ethereum);
    const [ids, setIds] = useState<any[]>([]);
    const [assignInput, setAssignInput] = useState<any>({});
    const [isLoding, setIsLoading] = useState<boolean>(false)
    const [successfullTx, setSuccessfullTx] = useState<boolean>(false)
    const walletContext = useContext(WalletContext)
    const getCHYPCIds = async (user: any) => {
        try {
            const cHYPCInstance = await getCHYPCContractInstance("0xAA36A7")
            const nftIds: any = await getNFTIdsAssignments(walletContext.state.wallet.accounts[0], cHYPCInstance);
            setIds(nftIds);
        } catch (error) {
            console.log(error)
        }
    }
    useEffect(() => {
        // Early returns for state inconsistencies
        if (walletContext.state.wallet.accounts.length == 0) return;
        getCHYPCIds(walletContext.state.wallet.accounts[0]);
    }, [walletContext.state.wallet.accounts]);

    const assignInputOnChange = (e: any, index: number) => {
        const value = e.replace(/[^0-9.]/g, "")
        setAssignInput({
            ...assignInput,
            [index]: value
        })
    }

    useEffect(() => {
        // Early returns for state inconsistencies
        if (walletContext.state.wallet.accounts.length == 0) return;
        let swapInstance: ethers.Contract;
        const getEvents = async () => {
            swapInstance = await getSwapContractInstance("0xAA36A7");
            //Get swaps for the user 
            const filterSwap = swapInstance.filters.Swap(walletContext.state.wallet.accounts[0])
            swapInstance.on(filterSwap, async (data: any) => {
                console.log(data, "Swap event data")
                await getCHYPCIds(walletContext.state.wallet.accounts[0]);
            })
        }
        getEvents();
        return () => {
            if (swapInstance === undefined) return;
            swapInstance.removeAllListeners()
        }
    }, [])
    
    useEffect(() => {
        // Early returns for state inconsistencies
        if (walletContext.state.wallet.accounts.length == 0) return;
        let chypcInstance: ethers.Contract;
        const getEvents = async () => {
            chypcInstance = await getCHYPCContractInstance("0xAA36A7");
            //Get swaps for the user 
            const filterAssign = chypcInstance.filters.Assigned(walletContext.state.wallet.accounts[0])
            chypcInstance.on(filterAssign, async (data: any) => {
                // await getCHYPCIds(walletContext.state.wallet.accounts[0]);
                console.log(data, "Assigned event data")
            })
        }
        getEvents();
        return () => {
            if (chypcInstance === undefined) return;
            chypcInstance.removeAllListeners()
        }
    }, [])
    
    const handleAssignClick = async (index: number, id: string) => {
        // Early return for mistaken clicks
        if(id === "") return;
        try {
            const signer = await provider.getSigner();
            const chypcInstance: any = await getCHYPCContractInstance("0xAA36A7");
            let assignTx = await chypcInstance.connect(signer).assign(id, assignInput[index])
            let assignReceipt = await assignTx.wait();
            if (assignReceipt.status) {
                console.log("success assignment")
                await getCHYPCIds(walletContext.state.wallet.accounts[0]);
            } else {
                ///Implement unsuccessful transaction response
                console.log("Unsuccess assignment")
            }
        } catch (error) {
            console.log(error)
        }
    }
    return (
        <>
            <div className='bg-[#162031]  w-[462px]  px-6 py-4 rounded-lg flex flex-col  items-center border border-gray-700 shadow-md shadow-gray-500/50'>
                <span className='w-full text-left mb-5'>
                    Assign your c_HYPC
                </span>
                {ids.length > 0 ?
                    ids.map((assignment: any, index) => {

                        return (<div key={assignment.id.toString()} className='bg-[#1e293b] text-gray-300 border border-gray-500 flex items-center w-full rounded-lg py-2 px-4 mb-3 shadow-md shadow-gray-500/50 hover:-translate-y-1 transition ease-in-out'>
                            <div className='flex flex-col'>
                                <span className='mb-2 font-semibold '>
                                    c_HYPC ID: {assignment.id.toString()}
                                </span>
                                <div className='flex w-full mb-1'>
                                    <input value={assignInput[index] || ""} onChange={e => assignInputOnChange(e.target.value, index)} type="text" name="token_out" autoComplete='off' autoCorrect='off' spellCheck="false" aria-label='tokenOut' placeholder="0" className='bg-[#334155] text-base rounded-md px-3 py-1 text-left outline-none input-number text-gray-300 mr-2' />
                                    <button onClick={() => handleAssignClick(index, assignment.id)} className='w-full flex justify-center py-1 px-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-gradient-to-tr from-purple-600 to-blue-500 hover:from-purple-500 hover:to-blue-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-violet-500'>Assign</button>
                                </div>
                                <span className='text-gray-600 text-sm font-extralight'>
                                    Assigned to: {assignment.assigned.toString() ? assignment.assigned.toString() : "None"}
                                </span>
                            </div>
                        </div>)
                    }) : ""
                }
            </div>
        </>
    );
};

export default Assigner;

