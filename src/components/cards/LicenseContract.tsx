import React, { useState, useEffect, useContext } from 'react';
import { BigNumberish, ethers, formatEther, formatUnits } from "ethers";
import { WalletContext } from 'context/WalletState';
import { getLicenseContractInstance, getLicenses } from 'connection';
import { SET_USER_LICENSES } from 'context/reducers';
import { HyperCycleLicense_Contract } from 'constants/addresses';

export interface ContractData {
    address: string;
    name: string;
    symbol: string;
    deployTime?: string;
    count: string;
    lastPurchase?: string;
    nextPrice: string;
    nextPriceBaseToken: string;
};

export const LicenseContract: React.FC<ContractData> = ({ address,
    name,
    symbol,
    deployTime,
    count,
    lastPurchase,
    nextPrice,
    nextPriceBaseToken }) => {
    const provider = new ethers.BrowserProvider(window.ethereum);
    const walletContext = useContext(WalletContext);
    const purchase = async () => {
        const signer = await provider.getSigner();
        const licenseInstance: any = await getLicenseContractInstance("0x13881");
        const licenseTx = await licenseInstance.connect(signer).purchase({ value: ethers.parseEther("0.02") })
        const licenseReceipt = await licenseTx.wait();
        if (licenseReceipt.status) {
            const licensesId = await getLicenses(walletContext.state.wallet.accounts[0], HyperCycleLicense_Contract['0x13881'])
            walletContext.dispatch({ type: SET_USER_LICENSES, payload: licensesId })
        }
    }

    return (
        <>
            <div className='border w-[512px] rounded-md py-7 px-5 text-white text-start bg-[#162031] border-gray-700 shadow-md shadow-gray-500/50  mb-4'>
                <div className='mb-5 flex flex-col'>
                    <div className='flex  font-bold mb-1'>
                        <span className='text-xl font-bold mr-3' >
                            {name}
                        </span>
                        <div className='text-left w-full text-gray-200 text-xl font-bold'>
                            ID: {count}
                        </div>
                    </div>
                    <span className='text-md font-light text-gray-400'>
                        {address}
                    </span>
                </div>
                <div className='mb-2'>
                    <div className='flex mb-1'>
                        <span className='text-md font-semibold text-gray-400 mr-3'> USD </span>
                        <span>{formatUnits(nextPrice, 18)}</span>
                    </div>
                    <div className='flex'>
                        <span className='text-md font-semibold mb-2 text-gray-400 mr-3'> Matic </span>
                        <span
                        >
                            {formatUnits(nextPriceBaseToken, 18)}
                        </span>
                    </div>
                </div>
                <div className='flex flex-col items-center justify-center'>
                    <button
                        onClick={purchase}
                        className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-lg font-medium text-white bg-gradient-to-tr from-purple-600 to-blue-500 hover:from-purple-500 hover:to-blue-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-violet-500"
                    >
                        Purchase
                    </button>
                </div>
            </div>
        </>
    );
};
