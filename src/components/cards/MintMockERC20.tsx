import React, { useState, useEffect, useContext } from 'react';
import LabeledDivider from 'components/utils/dividers/LabeledDivider';
import '../pages/Mint.css';
import { WalletContext } from 'context/WalletState';
import { ethers } from "ethers";
import { getERC20ContractInstance } from 'connection';

// interface SwapProps {
//     : string;
// }

const MintMockERC20: React.FC = () => {
    const [, set] = useState<string>();
    // useEffect(() => {
    //     set();
    // }, []);

    const { state, dispatch } = useContext(WalletContext);
    const walletContext = useContext(WalletContext);

    useEffect(() => {
        console.log(state);
    }, [walletContext.state.wallet.accounts]);

    const mintAMillion = async () => {
        const provider = new ethers.BrowserProvider(window.ethereum);
        const signer = await provider.getSigner();
        const erc20Instance: any = await getERC20ContractInstance("0xAA36A7");
        const mintTx = await erc20Instance.connect(signer).mintAMillion();
        const mintReceipt = await mintTx.wait()
        if(mintReceipt.status) {
            console.log("minting happened!")
        }
    }

    return (
        <>
            <h1 className='mint-heading'>Mint Mock HYPC Tokens</h1>
            <div className='bg-[transparent] w-[874px] px-6 py-4 rounded-lg  items-center grid'>
                <div className="grid-cols-12 grid">
                    <div className="col-span-8">
                        <input
                            type="text"
                            className="mint-input w-full h-[46px] rounded-xl bg-[#1e293b] text-white"
                            placeholder="Enter Your Wallet Address (0x...)"
                            value={state.wallet.accounts?.[0] || ""}
                            readOnly
                        />
                    </div>
                    <div className="col-span-4">
                        <button
                            type="submit"
                            className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-lg font-medium text-white bg-violet-600 hover:bg-violet-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-violet-500"
                            onClick={mintAMillion}
                        >
                            Mint
                        </button>
                    </div>
                </div>
            </div>
        </>
    );
};

export default MintMockERC20;

