import React from 'react';

interface LabeledDividerProps {
    label: string;
}

const LabeledDivider: React.FC<LabeledDividerProps> = ({ label }) => {
    return (
        <>
            <div className="relative w-full py-3">
                <div className="absolute inset-0 flex items-center justify-center w-full px-3" aria-hidden="true">
                    <div className="w-full border-t border-gray-300" />
                </div>
                <div className="relative flex justify-center">
                    <span className="px-2 text-sm text-gray-300 bg-[#162031]">{label}</span>
                </div>
            </div>
        </>
    );
};

export default LabeledDivider;
