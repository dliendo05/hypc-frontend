import React, { useContext, useEffect } from 'react';
import Navbar from './Navbar';
import Footer from './Footer';
import { WalletProvider, WalletContext } from 'context/WalletState';
import { BlockchainContext, BlockchainProvider } from 'context/BlockchainState';

const Layout = ({ children }: any) => {
    return (
        <div className='min-h-screen bg-[#0f172A] text-white'>
            <WalletProvider >
                    <Navbar />
                <BlockchainProvider >
                    {
                        children
                    }
                </BlockchainProvider>
                    <Footer />
            </WalletProvider>
        </div>
    );
};

export default Layout;
