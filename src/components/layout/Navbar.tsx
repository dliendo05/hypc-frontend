import React, { useState, useEffect, useContext, useLayoutEffect } from 'react';
import { Link, NavLink, useLocation } from 'react-router-dom';
import ConnectWallet from 'components/buttons/ConnectWallet';
import { WalletContext } from 'context/WalletState';
import { evmCompatibleConnector, isConnected, getMetamaskInfo, getLicenses } from 'connection';
import WalletAddress from 'components/buttons/WalletAddress';
import { HyperCycleLicense_Contract } from 'constants/addresses';
import { SET_USER_LICENSES, SET_WALLET } from 'context/reducers';

const Navbar: React.FC = () => {
    let location = useLocation();
    const { state, dispatch } = useContext(WalletContext)
    const [dropdown, setDropdown] = useState<boolean>(false);
    const [walletModal, setWalletModal] = useState<boolean>(false)
    const links = [
        { to: '/licenses', label: 'Licenses' },
        { to: '/swap', label: 'Swaps' },
        { to: '/mint', label: 'Mint' },
    ]
    let chain;

    const connectWallet = async () => {
        //Setting testnets
        if (location.pathname == "/licenses") {
            const { metamaskInfo } = await evmCompatibleConnector('0x13881')
            dispatch({ type: SET_WALLET, payload: metamaskInfo })
            const licensesId = await getLicenses(metamaskInfo!.accounts[0], HyperCycleLicense_Contract['0x13881'])
            dispatch({ type: SET_USER_LICENSES, payload: licensesId })
        } else if (location.pathname == "/swap") {
            const { metamaskInfo } = await evmCompatibleConnector("0xAA36A7")
            dispatch({ type: SET_WALLET, payload: metamaskInfo })
        } else if(location.pathname == "/mint") {
            const { metamaskInfo } = await evmCompatibleConnector("0xAA36A7")
            dispatch({ type: 'SET_WALLET', payload: metamaskInfo })
        }
        }

    useLayoutEffect(() => {
        const checkIfConnect = async () => {
            if (await isConnected()) {
                await connectWallet()
            }
        }
        checkIfConnect()
    }, [location.pathname])


    return (
        <>

            <nav className="border-b-2 border-gray-800 bg-gray-900">
                <div className="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
                    <Link to="/" className="flex items-center">
                        <img src="https://static.wixstatic.com/media/f8ff07_d2a80c8471a543fe957466713cd4f14f~mv2.png/v1/fill/w_131,h_131,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/HyperCycle-Logo-2022_icon%20white.png" alt="HyperCycle-Logo-2022_icon white.png" className="w-8 h-8 mr-2 object-cover object-center" />
                        <span className="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">HyperCycle</span>
                    </Link>
                    <button data-collapse-toggle="navbar-multi-level" type="button" className="inline-flex items-center p-2 ml-3 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600" aria-controls="navbar-multi-level" aria-expanded="false" onClick={() => setDropdown(!dropdown)}>
                        <span className="sr-only">Open main menu</span>
                        <svg className="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clipRule="evenodd"></path></svg>
                    </button>
                    <div className="hidden w-full md:block md:w-auto" id="navbar-multi-level">
                        <ul className="flex flex-col items-center font-medium p-4 md:p-0 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
                            {
                                state.wallet.accounts.length === 0 ? <ConnectWallet isOpen={walletModal} setIsOpen={setWalletModal} onClick={connectWallet} />
                                    : <div>
                                        < WalletAddress />
                                    </div>
                            }
                            <li>
                                <NavLink to="/licenses" className={({ isActive, isPending }) => isActive ? "text-blue-700" : "text-gray-900" + "block py-2 pl-3 pr-4  rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent"}>Licenses</NavLink>
                            </li>
                            <li>
                                <NavLink to="/swap" className={({ isActive, isPending }) => isActive ? "text-blue-700" : "text-gray-900" + "block py-2 pl-3 pr-4  rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent"}>Swap</NavLink>
                            </li>
                            <li>
                                <NavLink to="/mint" className={({ isActive , isPending}) => isActive ? "text-blue-700" : "text-gray-900" + "block py-2 pl-3 pr-4  rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent"}>Mint</NavLink>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

        </>
    );
};

export default Navbar;
