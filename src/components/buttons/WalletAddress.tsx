import React, { useState, useEffect, useContext } from 'react';
import { WalletContext } from 'context/WalletState';

const WalletAddress: React.FC = () => {
    const [, set] = useState<string>();
    const { state, dispatch } = useContext(WalletContext)

    return (
        <>
        <div className='text-md font-bold bg-[#1e293b] px-4 py-2 rounded-md'>
            {
                state.wallet.accounts[0].slice(0,6) + "..." + state.wallet.accounts[0].slice(state.wallet.accounts[0].length - 4,state.wallet.accounts[0].length)
            }

        </div>
        </>
    );
};

export default WalletAddress;
