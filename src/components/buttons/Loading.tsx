import Spinner from 'components/loaders/Spinner';
import React from 'react';

interface LoadingProps {
    classes: string;
    size: string
}

const Loading: React.FC<LoadingProps> = ({ classes, size }) => {
    return (
        <>
            <button className={classes} >
                <Spinner size={size} />
                <span className=''>
                    Waiting for transaction...
                </span>
            </button>
        </>
    );
};

export default Loading;
