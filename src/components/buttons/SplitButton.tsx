import React from 'react';

interface AppProps {
    licenseId: string;
}

const App: React.FC<AppProps> = ({ licenseId }) => {
    const splitlicense = async () => {
    }
    return (
        <button
            onClick={splitlicense}
            className="flex justify-center py-1 px-4 border border-transparent rounded-md shadow-sm text-md font-light text-white bg-gradient-to-tr from-purple-600 to-blue-500 hover:from-purple-500 hover:to-blue-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-violet-500"
        >
            Split
        </button>
    );
};

export default App;
