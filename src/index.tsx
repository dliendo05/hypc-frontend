import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import {
  createBrowserRouter,
  RouterProvider,
  BrowserRouter as Router,
  Routes,
  Route,
  Outlet
} from "react-router-dom";
import License from "components/pages/License";
import Swap from "components/pages/Swap";
import Home from "components/pages/Home";
import Layout from "components/layout";
import Error from "components/pages/Error";
import Mint from "components/pages/Mint";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <React.StrictMode >
    <Router>
      <Routes>
        <Route element={
          <Layout><Outlet /></Layout>
        }>
          <Route path="/" element={<Home />} />
          <Route path="/licenses" element={<License />} />
          <Route path="/swap" element={<Swap />} />
          <Route path="/mint" element={<Mint />} />
          <Route path="*" element={<Error />} />
          <Route path="/mint" element={<Mint />} />
        </Route>
      </Routes>
    </Router>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
